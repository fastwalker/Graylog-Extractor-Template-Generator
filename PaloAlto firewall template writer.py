import TemplateWriter


# Reference:
# https://www.paloaltonetworks.com/documentation/80/pan-os/pan-os/monitoring/use-syslog-for-monitoring/syslog-field-descriptions/threat-log-fields#id83052cb2-4798-4f9c-abf8-e0b929ce7a3b


def write_threat_templates():

    threat_fields = 'FUTURE_USE, Receive Time, Serial Number, Type, Threat or Content Type, FUTURE_USE, Generated Time, Source IP, Destination IP, NAT Source IP, NAT Destination IP, Rule Name, Source User, Destination User, Application, Virtual System, Source Zone, Destination Zone, Inbound Interface, Outbound Interface, Log Action, FUTURE_USE, Session ID, Repeat Count, Source Port, Destination Port, NAT Source Port, NAT Destination Port, Flags, Protocol, Action, URL or Filename, Threat ID, Category, Severity, Direction, Sequence Number, Action Flags, Source Location, Destination Location, FUTURE_USE, Content Type, PCAP_ID, File Digest, Cloud, URL Index, User Agent, File Type, X-Forwarded-For, Referer, Sender, Subject, Recipient, Report ID, Device Group Hierarchy Level 1, Device Group Hierarchy Level 2, Device Group Hierarchy Level 3, Device Group Hierarchy Level 4, Virtual System Name, Device Name, FUTURE_USE, Source VM UUID, Destination VM UUID, HTTP Method, Tunnel ID or IMSI, Monitor Tag or IMEI, Parent Session ID, Parent Start Time, Tunnel Type, Threat Category, Content Version, FUTURE_USE'
    fields = [x.strip() for x in threat_fields.split(',')]
    TemplateWriter.write_templates(extractor_fields=fields, title_prefix='Threat',
                                   condition_value="\\\\,.+\\\\,.+\\\\,THREAT")


def write_system_templates():
    system_fields = 'FUTURE_USE, Receive Time, Serial Number, Type, Content/Threat Type, FUTURE_USE, Generated Time, Virtual System, Event ID, Object, FUTURE_USE, FUTURE_USE, Module, Severity, Description, Sequence Number, Action Flags, Device Group Hierarchy Level 1, Device Group Hierarchy Level 2, Device Group Hierarchy Level 3, Device Group Hierarchy Level 4, Virtual System Name, Device Name'
    fields = [x.strip() for x in system_fields.split(',')]
    TemplateWriter.write_templates(extractor_fields=fields, title_prefix='System',
                                   condition_value="\\\\,.+\\\\,.+\\\\,SYSTEM")


if __name__ == '__main__':
    
    write_system_templates()
