"""
Script to generate extractor templates for Graylog. Currently only generates "split and index" templates. More types
will be added as I need them; feel free to make a pull request if you extend the generator for other types of
templates.
Author:
"""


prefix = """
         {
  "extractors": [\n
"""

postfix = """
  ],
  "version": "2.4.5"
}
"""

threat_template = """
   {
      "title": "%TITLE_PREFIX% - %TITLE%",
      "extractor_type": "%EXTRACTOR_TYPE%",
      "converters": %CONVERTERS%,
      "order": %ORDER%,
      "cursor_strategy": "%CURSOR_STRATEGY%",
      "source_field": "%SOURCE_FIELD%",
      "target_field": "%FIELD_NAME%",
      "extractor_config": {
        "index": %INDEX%,
        "split_by": "%SPLIT_BY%"
      },
      "condition_type": "%CONDITION_TYPE%",
      "condition_value": "%CONDITION_VALUE%"
    }"""


def write_templates(extractor_fields, title_prefix, condition_value, extractor_type="split_and_index", converters='[]',
                    cursor_strategy='copy',  source_field='message', split_by=',', condition_type='regex'):

    with open('import_file', 'w') as ofile:
        ofile.write(prefix)
        index = 0
        for field in extractor_fields:

            index += 1
            if field == 'FUTURE_USE':
                continue
            title = field
            title_prefix = title_prefix
            order = index - 1
            field_name = field.lower().replace(' ', '_').strip()

            generated_template = threat_template
            generated_template = generated_template.replace('%TITLE_PREFIX%', title_prefix)
            generated_template = generated_template.replace('%TITLE%', title)
            generated_template = generated_template.replace('%ORDER%', str(order))
            generated_template = generated_template.replace('%FIELD_NAME%', field_name)
            generated_template = generated_template.replace('%INDEX%', str(index))
            generated_template = generated_template.replace('%CONDITION_VALUE%', condition_value)
            generated_template = generated_template.replace('%EXTRACTOR_TYPE%', extractor_type)
            generated_template = generated_template.replace('%CONVERTERS%', converters)
            generated_template = generated_template.replace('%CURSOR_STRATEGY%', cursor_strategy)
            generated_template = generated_template.replace('%SOURCE_FIELD%', source_field)
            generated_template = generated_template.replace('%SPLIT_BY%', split_by)
            generated_template = generated_template.replace('%CONDITION_TYPE%', condition_type)
            ofile.write(generated_template + (',\n' if index < len(extractor_fields) else '\n'))
        ofile.write(postfix)